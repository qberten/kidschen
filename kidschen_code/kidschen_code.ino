// Kidschen_code

#define BUZZER_PIN A0

// Config of the rotary encode
// For cabling, see https://bokc-fr.blogspot.com/2014/09/arduino-encodeur-rotatif-ky040.html 
#define CLK_PIN 3
#define DT_PIN 2
#define SW_PIN A3
#define ENCODER_MAIN_TICKS 4 // 4 ticks of encoder readings for a main tick on the encoder
long rotEncPos = 0;
long rotEncPosOld = 0;
bool isCW = false;
bool isCCW = false;

// The time of the clock timer
int clockTime = 0;
#define CLOCK_TIME_MAX 80 // The maximum time (20 sec * 4)
#define CLOCK_TIME_MIN 0 // The minimum time (20 sec * 4)

// Definition of clock internal states
#define STATE_COUNTING 11
#define STATE_PAUSE 22
#define STATE_ZERO 33
volatile byte prgState = STATE_ZERO;

// Variable to manage pin 13 brightness
int ledFadeAmount = 5;
int ledBrightness = 0;
#define LED_STATUS_PIN 13 // use builtin led 13

// The external button pin
#define BUT_PIN A5

#include <SevSeg.h>
#include <EasyButton.h>
#include <Encoder.h>

SevSeg sevseg;	   // Instantiate a seven segment controller object 
// Instantiate the swButton, debounce 35ms, pullup enable, inverted logic = true
EasyButton swButton(SW_PIN, 35, true, true);
// Instantiate the external Button, debounce 35ms, pullup enable, inverted logic = true
EasyButton extButton(BUT_PIN, 35, true, true);
// Instantiate the encoder (2 and 3 are pins with interrupts)
Encoder rotEnc(3, 2);

// Config of the buzzer
void setup() {
    
    // Config and initialisation of the 4 digits 7 segments display
    // For cabling, see http://haneefputtur.com/7-segment-4-digit-led-display-sma420564-using-arduino.html
    byte numDigits = 4;
    byte digitPins[] = {10, 11, 12, A4}; //Digits: 1,2,3,4, with 220 Ohms/330 Ohms on each digit pin)
    byte segmentPins[] = {A2, A1, 4, 5, 6, 7, 8, 9};
    bool resistorsOnSegments = false; // 'false' means resistors are on digit pins
    byte hardwareConfig = COMMON_CATHODE;
    bool updateWithDelays = false; // Default. Recommended
    bool leadingZeros = false; // Use 'true' if you'd like to keep the leading zeros
    sevseg.begin(hardwareConfig, numDigits, digitPins, segmentPins, resistorsOnSegments, updateWithDelays, leadingZeros);
    sevseg.setBrightness(10);

    // Setup of the internal LED (pin 13), for debug purpose
    pinMode(LED_STATUS_PIN, OUTPUT);

    // initialise rotary encoder internal button
    swButton.begin();
    // add callback function
    swButton.onPressed(swBtnPressed);

    // initialise external button
    extButton.begin();
    // add callback function
    extButton.onPressed(extButtonPressed);
}

void switchPrgState() {
    switch (prgState) { 
	case STATE_PAUSE:
	    prgState = STATE_COUNTING;
	    break;
	case STATE_COUNTING:
	    // reset encoder value (to avoid counting the turns while counting)
	    rotEnc.write(rotEncPos);
	    prgState = STATE_PAUSE;
	    break;
	case STATE_ZERO:
	    // Already at zero
	    break;
	default:
	    // ledBrightness = 0;
	    break;
    }
 
}

void resetPrgState() {
    prgState = STATE_PAUSE;
    clockTime = 0;
    rotEncPos = 0;
    rotEnc.write(rotEncPos);
    rotEncPosOld = 0;
}

void swBtnPressed() {
    tone(BUZZER_PIN, 1000, 10);
    switchPrgState();
}

void extButtonPressed() {
    tone(BUZZER_PIN, 2000, 200);
    resetPrgState();
}

// the loop function runs over and over again forever
void loop() {
    static unsigned long timer = millis();
    static int deciSeconds = 0;
  
    if (millis() >= timer) {
	deciSeconds++; // 100 milliSeconds is equal to 1 deciSecond
	timer += 100; 
	if (deciSeconds == 10000) { // Reset to 100 after counting for 1000 seconds.
	    deciSeconds=100;
	}
	if ((deciSeconds % 10 == 0) && (prgState == STATE_COUNTING)) { 
	    // decreasing every secs
	    if (clockTime >= (CLOCK_TIME_MIN + ENCODER_MAIN_TICKS)) {
		clockTime -= ENCODER_MAIN_TICKS;
		tone(BUZZER_PIN, 1109, 10);
	    } else {
		// at zero, => change stop counting
		// Play nice tone
		clockTime = 0;
		tone(BUZZER_PIN, 2000, 500);
	    }
	}
    }

    // Read rotary encoder if we are on pause or at zero
    if (prgState == STATE_PAUSE || prgState == STATE_ZERO) {
	rotEncPos = rotEnc.read();
	isCW = (rotEncPos > rotEncPosOld);
	isCCW = (rotEncPos < rotEncPosOld);
	clockTime += rotEncPos - rotEncPosOld;
	rotEncPosOld = rotEncPos;
	if (isCW){
	    // rotated clockwise
	    tone(BUZZER_PIN, 3000, 10);
	} else if (isCCW) {
	    // rotated counterclockwise
	    tone(BUZZER_PIN, 500, 10);
	}
    }

    // Cap clockTime between CLOCK_TIME_MIN and CLOCK_TIME_MAX
    if (clockTime >= CLOCK_TIME_MAX) {
	clockTime = CLOCK_TIME_MAX;
    } else if (clockTime <= CLOCK_TIME_MIN) {
	prgState = STATE_ZERO;
	clockTime = CLOCK_TIME_MIN;
    } else {
	if (prgState == STATE_ZERO) {
	    prgState = STATE_PAUSE;
	}
    }

    sevseg.setNumber((int)clockTime/ENCODER_MAIN_TICKS,0); // rotaty encoder is 4 steps for 1 main click
    sevseg.refreshDisplay(); // Must run repeatedly
    swButton.read();
    extButton.read();

    // Set Status Led
    switch (prgState) { 
	case STATE_PAUSE:
	    ledBrightness = 0;
	    break;
	case STATE_COUNTING:
	    if (deciSeconds % 3 == 0) {
		ledBrightness = 100;
	    } else {
		ledBrightness = 0;
	    }
	    break;
	case STATE_ZERO:
	    ledBrightness = 100;
	    break;
	default:
	    // ledBrightness = 0;
	    break;
    }
    
    analogWrite(LED_STATUS_PIN, ledBrightness);

}

/* vim: set ts=8 sw=4 tw=0 noet : */
